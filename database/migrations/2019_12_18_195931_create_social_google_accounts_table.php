<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialGoogleAccountsTable extends Migration
{
    
    public function up()
    {
        Schema::create('social_google_accounts', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->integer('user_id');
            $table->string('provider_user_id');
            $table->string('provider');

            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('social_google_accounts');
    }
}
