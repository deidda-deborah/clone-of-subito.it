<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnouncementImagesTable extends Migration
{
    
    public function up()
    {
        Schema::create('announcement_images', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('file');

            $table->unsignedBigInteger('announcement_id');
            $table->foreign('announcement_id')->references('id')->on('announcements');

            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('announcement_images');
    }
}
