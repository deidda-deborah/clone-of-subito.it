<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSocialFieldsToUsersTable extends Migration
{
    
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->string('provider')->nullable();
            $table->unsignedBigInteger('provider_id')->nullable();
        });
    }

    
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->dropColumn('provider');
            $table->dropColumn('provider_id');
        });
    }
}
