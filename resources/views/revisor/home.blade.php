@extends('layouts.app')
@section('content')
<section class="container">
        <div class="row m-1">
            <div class="col-12 col-md-6 text-center mx-auto">
                <h1>Revisor home</h1>
            </div>
        </div>
    @if($announcement)
    <div class="card">
        <div class="row no-gutters m-1">
            @foreach($announcement->images as $image)
                <div class="col-12">
                        <img src="{{ $image->getUrl(300, 150) }}" class="card-img" alt="Lorem picsum">
                    <div class="col-12 col-md-6 m-2 text-center mx-auto">
                        <strong>Adult:</strong><em> {{ $image->adult }} </em><br>
                        <strong>Medical:</strong><em> {{ $image->medical }} </em><br>
                        <strong>Spoof:</strong><em> {{ $image->spoof }} </em><br>
                        <strong>Violence:</strong><em> {{ $image->violence }} </em><br>
                        <strong>Racy:</strong><em> {{ $image->racy }} </em><br>
                    </div>
                    <br>
                    <div class="col-12 col-md-6 text-center mx-auto">
                        <strong>Labels</strong><br>
                        <ul>
                            @if($image->labels)
                                @foreach($image->labels as $label)
                                    <li> {{ $label }} </li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    {{-- {{ $image->id }} <br>
                    {{ $image->file }} <br>
                    {{ Storage::url($image->file) }} --}}
                </div>
            @endforeach
            <div class="col-12 col-md-6 text-center mx-auto">
                <div class="card-body">
                <h4>Announcement # {{$announcement->id}}</h4>
                <h5 class="card-title">{{$announcement->title}}</h5>
                <p class="card-text">{{$announcement->description}}</p>
                <p class="card-text"><i>{{$announcement->created_at->format('d/m/Y')}} - {{$announcement->user->name}} {{$announcement->email}}</i></p>
                </div>
            </div>
        </div>
        <div class="row m-1">
            <div class="col-12 col-md-6">
                <form action="{{route('revisor.accept', $announcement->id)}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-outline-secondary">Accept</button>
                </form>
            </div>
            <div class="col-12 col-md-6 text-right">
                <form action="{{route('revisor.reject', $announcement->id)}}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-outline-danger">Reject</button>
                </form>
            </div>
        </div>
    </div>
    @else
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="alert alert-dark"><h4>There are no announcements to review.</h4>
                </div>
            </div> 
        </div>
    @endif 
</section>
@endsection