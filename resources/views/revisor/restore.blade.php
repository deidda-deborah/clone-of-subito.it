@extends('layouts.app')
@section('content')
<section class="container">
    <div class="row text-center mx-auto">
        <div class="col-12 col-md-6 mx-auto">
            <h1>Restore</h1>
        </div>
    </div>
</section>
@if($announcements)
<section class="container">
    <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
            <div class="col-12 mx-auto m-2">
                @foreach($announcements as $announcement)
                <div class="card mb-3 text-center mx-auto" style="max-width: 600px;">
                    @foreach($announcement->images as $image)
                    <div class="row">
                        <div class="col-12">
                            <img src="{{ $image->getUrl(300, 150) }}" class="card-img" alt="Lorem picsum">
                        </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <div class="col-12">
                            <div class="card-body">
                                <h5 class="card-title">{{$announcement->title}}</h5>
                                <p class="card-text">{{$announcement->description}}</p>
                                <p class="card-text"><strong>Category: <a href="{{route('announcements.bycategory', [$announcement->category->name, $announcement->category->id])}}">{{$announcement->category->name}}</a></strong></p>
                                <p class="card-text"><i>{{$announcement->created_at->format('d/m/Y')}} - {{$announcement->user->name}}</i></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 text-left">
                            <form action="{{route('revisor.accept', $announcement->id)}}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-outline-secondary">Restore</button>
                            </form>
                        </div>
                        <div class="col-12 col-md-6 text-right">
                            <form action="{{route('revisor.reject', $announcement->id)}}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-outline-danger">Reject definitely</button>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>   
        </div>
    </div>
</section>
@else
<section class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="alert alert-dark"><h4>There are no announcements to review.</h4></div>
            </div> 
        </div>
</section>
@endif

@endsection