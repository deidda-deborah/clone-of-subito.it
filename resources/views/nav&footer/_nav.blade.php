<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto my-auto">
                <li class="nav-item my-auto mx-3">
                    <a class="navbar-brand" href="{{ url('/') }}"><strong>Presto.it</strong></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span></button>
                </li>
                <li class="nav-item mx-3 my-auto text-center">
                <a class="nav-link" href="{{ route('all') }}">All announcements</a>
                </li>
                @guest
                    <li class="nav-item mx-3 my-auto text-center">
                        <a class="nav-link" href="{{ route('register') }}">New Announce</a>
                    </li>
                @else
                    <li class="nav-item mx-3 my-auto">
                        <a class="nav-link" href="{{ route('announcements.new') }}">New Announce</a>
                    </li>
                @endguest
                <li class="nav-item dropdown mx-3 my-auto">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category</a>
                    
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach($categories as $category)
                            <a class="dropdown-item" href="{{route('announcements.bycategory', [$category->name, $category->id])}}">{{$category->name}}</a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item mx-3 my-auto">
                    @include('nav&footer._locale', ['language' => 'it', 'nation' => 'it'])
                </li>
                <br>
                <li class="nav-item mx-3 my-auto">
                    @include('nav&footer._locale', ['language' => 'en', 'nation' => 'gb'])
                </li>
                <br>
                <li class="nav-item mx-3 my-auto">
                    @include('nav&footer._locale', ['language' => 'es', 'nation' => 'es'])
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    <li class="nav-item mx-3 text-center">
                        <a class="nav-link" href="{{route('login.social',['service'=>'facebook'])}}">Facebook login</a>
                    </li>
                    <li class="nav-item mx-3 text-center">
                        <a class="nav-link" href="{{url('/redirect')}}">Google login</a>
                    </li>
                    @if (Route::has('register'))
                    <li class="nav-item mx-3">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                    @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                    @if(Auth::user()->is_revisor)
                <li class="nav-item dropdown mx-3 my-auto">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Revisor</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('revisor.home')}}">To review<span class="badge badge-pill badge-warning">{{\App\Announcement::ToBeRevisionedCount()}}</span></a>
                        <a class="dropdown-item" href="{{route('revisor.restore')}}">Restore<span class="badge badge-pill badge-warning">{{\App\Announcement::ToBeRevisionedCount()}}</span></a>
                    </div>
                </li>
                    @endif
                @endguest  
            </ul>
        </div>
    </div>
</nav>
