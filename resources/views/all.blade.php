@extends('layouts.app')
@section('content')
<section class="container">
    <div class="row text-center mx-auto">
        <div class="col-12 col-md-6 text-center mx-auto">
            <h2>All announcements</h2>
            @include('announcements._announcement')
            <br>
            {{ $announcements->links() }}
        </div>
    </div>
</section>
@endsection
    