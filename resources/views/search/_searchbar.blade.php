@section('css')
<style>
.main {
    width: 50%;
    margin: 50px auto;
}

.has-search .form-control {
    padding-left: 2.375rem;
}

.has-search .form-control-feedback {
    position: absolute;
    z-index: 2;
    display: block;
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    text-align: center;
    pointer-events: none;
    color: #aaa;
}
</style>
@endsection
<div class="container">
    <form action="{{route('search')}}" method="get">
        @csrf
        <div class="row input-group text-center mx-auto">
            <div class="col-md-6 py-4 input-group-append text-center mx-auto">     
                <input class="form-control" type="text" name="q" placeholder="Search">
                <button type="submit" class="btn btn-secondary"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </form>
</div>
