@extends('layouts.app')
@section('content')
<section class="container">
    <div class="row text-center mx-auto">
        <div class="col-12 col-md-6 text-center mx-auto">
            <h1>Risultati ricerca per categoria: <i>{{ $q }}</i></h1>
            @include('announcements._announcement')
        </div>
    </div>  
</section>
@endsection