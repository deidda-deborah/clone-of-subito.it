@extends('layouts.app')
@section('content')
<section class="container">
    <div class="row">
        <div class="col-12 col-md-6 text-center mx-auto">
            <h3>Announcement for category: {{$category->name}}</h3>
            @include('announcements._announcement')
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4">
            {{ $announcements->links() }}
        </div>
    </div>
</section>
@endsection