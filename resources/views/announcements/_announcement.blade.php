@foreach($announcements as $announcement)
    <div class="card mb-3 text-center mx-auto" style="max-width: 600px;">
        <div class="row">
            @foreach($announcement->images as $image)
                <div class="col-12 text-center">
                    <img src="{{ $image->getUrl(300, 150) }}" class="card-img" alt="Lorem picsum">
                </div>
            @endforeach
            <div class="col-12 text-center">
                <div class="card-body">
                    <h5 class="card-title">{{$announcement->title}}</h5>
                    <p class="card-text">{{$announcement->description}}</p>
                    <p class="card-text"><strong>Category: <a href="{{route('announcements.bycategory', [$announcement->category->name, $announcement->category->id])}}">{{$announcement->category->name}}</a></strong></p>
                    <p class="card-text"><i>{{$announcement->created_at->format('d/m/Y')}} - {{$announcement->user->name}}</i></p>
                </div>
            </div>
        </div>
    </div>
@endforeach