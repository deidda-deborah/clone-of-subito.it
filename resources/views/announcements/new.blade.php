@extends('layouts.app')
@section('content')
<section class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6 text-center">
            <div class="my-3">
                <h2>Write your announce</h2>
            </div>
        {{-- <h3>DEBUG:: SECRET {{ $uniqueSecret }}</h3> --}}
            @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
            <form method="POST" action="{{ route('announcements.create') }}">
                @csrf
                <input 
                type="hidden"
                name="uniqueSecret"
                value="{{ $uniqueSecret }}">
                
                    <div class="form-group row">
                        <label for="category" class="col-md-4 col-form-label text-md-right">Category</label>
                        <div class="col-md-6">
                            <select id="category" class="form-control" name="category">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="title" class="col-md-4 col-form-label text-md-right">Title</label>
                        <div class="col-md-6">
                            <input id="title" type="title" class="form-control" name="title" value="{{ old('title') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>
                        <div class="col-md-6">
                            <input id="description" type="description" class="form-control" name="description" value="{{ old('description') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="images" class="col-md-4 col-form-label text-md-center mx-auto">Add images</label>
                        <div class="col-12">
                            <div class="dropzone" id="drophere"></div>
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-outline-secondary px-5"><big>Invia</big></button>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</section>
@endsection