@extends('layouts.app')
@section('content')
<section class="container">
    <div class="row text-center mx-auto">
        <div class="col-12 col-md-6 text-center mx-auto">
            @if(session('announcement.created.success'))
            <div class="alert alert-info" role="alert">
                Your announce has been created successfully!
            </div>
            @endif
            @if(session('access.denied.revisor.only'))
            <div class="alert alert-danger" role="alert">
                Access denied: only revisors.
            </div>
            @endif
            <h1>{{__('ui.welcome')}}</h1>
            <h2>Last announcements</h2>
            @include('announcements._announcement')
        </div>
    </div>
</section>
@endsection
    