<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class MakeUserRevisor extends Command
{
    
    protected $signature = 'presto:makeUserRevisor';

    
    protected $description = 'Make a reviewer user';

    
    public function __construct()
    {
        parent::__construct();
    }

    
    public function handle()
    {
        $email = $this->ask("Enter the email address of the user you want to make a reviewer");
        $user = User::where('email', $email)->first();

        if(!$user){

            $this->error("User not found");
            return;
        }

        $user->is_revisor = true;
        $user->save();
        $this->info("The user: {$user->name}, is now reviewer.");
    }
}
