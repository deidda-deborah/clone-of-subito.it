<?php

namespace App\Http\Controllers;

use session;
use App\Category;
use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class FrontController extends Controller
{
    public function homeAllAnnouncements()
    {
        $announcements = Announcement::where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        ->take(5)
        ->get();
        return view('home', compact('announcements'));
    }

    public function newAnnouncement(Request $request)
    {
        $uniqueSecret = $request->old(

            'uniqueSecret',
            base_convert(sha1(uniqid(mt_rand())), 16, 36)
        ); 

        return view('announcements.new', compact('uniqueSecret'));
    }

    public function search(Request $request){

        $q = $request->input('q');

        $announcements = Announcement::search('q')
        ->where('is_accepted', true)
        ->get();

        return view('search.search_results', compact('q', 'announcements'));
    }

    public function locale($locale){

        session()->put('locale', $locale);
        return redirect()->back();
    }

    
}
