<?php

namespace App\Http\Controllers;


use App\Category;
use App\Announcement;
use App\Jobs\ResizeImage;
use App\AnnouncementImage;
use Illuminate\http\Request;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\NewAnnouncementRequest;

class AnnouncementsController extends Controller
{
    public function createAnnouncement(NewAnnouncementRequest $request)
    {
        $new = new Announcement(); 

        $new->title = $request->input('title'); 
        $new->description = $request->input('description');
        $new->category_id = $request->input('category');

        $new->user_id = Auth::user()->id;

        $new->save();

        $uniqueSecret = $request->input('uniqueSecret');
        
        $images = session()->get("images.{$uniqueSecret}", []);  

        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        foreach($images as $image){

            $i = new AnnouncementImage();

            $fileName = basename($image);
            
            $newFileName = "public/announcements/{$new->id}/{$fileName}";

            Storage::move($image, $newFileName);

            $i->file = $newFileName;

            $i->announcement_id = $new->id;

            $i->save();

            GoogleVisionSafeSearchImage::withChain([

                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemoveFaces($i->id),
                new ResizeImage($i->file, 300, 150)

            ])->dispatch($i->id);

        }

        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
        
        return redirect('/')->with('announcement.created.success', 'ok');
    }

    public function announcementsByCategory($name, $category_id){

        $category = Category::find($category_id);

        $announcements = $category->announcements()
        ->where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        return view('announcements.bycategory', compact('category', 'announcements'));
    }

    public function uploadImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->file('file')->store("public/temp/{ $uniqueSecret }");

        dispatch(new ResizeImage(

            $fileName,
            120,
            120
        ));

        session()->push("images.{$uniqueSecret}", $fileName);

        return response()->json(
 
            [
                'id' => $fileName
            ]
        );
    }

    public function removeImage(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $fileName = $request->input('id');

        session()->push("removedimages.{$uniqueSecret}", $fileName);

        Storage::delete($fileName);

        return response()->json('ok');

    }

    public function getimages(Request $request){

        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);      
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data = [];

        foreach($images as $image){

            $data[] = [

                'id' => $images,
                'src' => AnnouncementImage::getUrlByFilePath($image, 120, 120)
            ];
        }

        return response()->json($data);
    }

    public function allAnnouncements(){

        $announcements = Announcement::where('is_accepted', true)
        ->orderBy('created_at', 'desc')
        ->paginate(5);

        return view('all', compact('announcements'));
    }
}
