<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{

    use Searchable;

    protected $fillable = ['title', 'description'];

    public function toSearchableArray()
    {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'category' => $this->category,
            'other' => 'casa motori giochi antiquariato',
        ];
    
        return $array;
    }

    public function category(){

        return $this->belongsTo(Category::class);
    }

    public function user(){

        return $this->belongsTo(User::class);
    }

    static public function ToBeRevisionedCount(){

        return Announcement::where('is_accepted', null)->count();
    }

    public function images(){

        return $this->hasMany(AnnouncementImage::class);
    }

    
}
