<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//front
Route::get('/', 'FrontController@homeAllAnnouncements')->name('home');
Route::get('/announcement/new', 'FrontController@newAnnouncement')->name('announcements.new');
Route::get('/search', 'FrontController@search')->name('search');
Route::post('/locale/{locale}', 'FrontController@locale')->name('locale');

//announcements
Route::post('/announcement/create', 'AnnouncementsController@createAnnouncement')->name('announcements.create');
Route::get('/category/{name}/{id}/announcements', 'AnnouncementsController@announcementsByCategory')->name('announcements.bycategory');
Route::get('/announcements/all', 'AnnouncementsController@allAnnouncements')->name('all');

//dropzone
Route::post('/announcement/images/upload', 'AnnouncementsController@uploadImage')->name('announcements.images.upload');
Route::delete('/announcement/images/remove', 'AnnouncementsController@removeImage')->name('announcements.images.remove');
Route::get('/announcement/images', 'AnnouncementsController@getImages')->name('announcements.images');

//revisor area
Route::get('/revisor/home', 'RevisorController@homeRevisor')->name('revisor.home');
Route::get('/revisor/home/restore', 'RevisorController@restore')->name('revisor.restore');

Route::post('/revisor/announcement/{id}/accept', 'RevisorController@accept')->name('revisor.accept');
Route::post('/revisor/announcement/{id}/reject', 'RevisorController@reject')->name('revisor.reject');

//socialite facebook
Route::get ( '/redirect/{service}', 'SocialAuthController@redirect' )->name('login.social');
Route::get ( '/callback/{service}', 'SocialAuthController@callback' );

//socialite google
Route::get('/redirect', 'SocialAuthGoogleController@redirect');
Route::get('/callback', 'SocialAuthGoogleController@callback');

